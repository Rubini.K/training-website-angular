import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCoursetypeComponent } from './home-coursetype.component';

describe('HomeCoursetypeComponent', () => {
  let component: HomeCoursetypeComponent;
  let fixture: ComponentFixture<HomeCoursetypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeCoursetypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCoursetypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
