import { UpcommingProgramService } from './../../services/upcomming-program.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-add-program',
  templateUrl: './add-program.component.html',
  styleUrls: ['./add-program.component.css']
})
export class AddProgramComponent implements OnInit {
  program = {
    programName:'',
    programDescription:'',
    programType:'',
    programStartdate:'',
    programPrice:'',
    published:false
  };
  submitted=false;

  constructor( private upcommingProgramService: UpcommingProgramService) { }

  ngOnInit(): void {
  }

  saveProgram(): void {
    const data = {
      programName: this.program.programName,
      programDescription: this.program.programDescription,
      programType: this.program.programType,
      programStartdate: this.program.programStartdate,
      programPrice: this.program.programPrice
    };

    this.upcommingProgramService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newProgram(): void {
    this.submitted = false;
    this.program = {
      programName: '',
      programDescription: '',
      programType:'',
      programStartdate:'',
      programPrice:'',
      published: false
    };
  }

}
