import { Component, OnInit } from '@angular/core';
import { UpcommingProgramService } from 'src/app/services/upcomming-program.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-programs-details',
  templateUrl: './programs-details.component.html',
  styleUrls: ['./programs-details.component.css']
})
export class ProgramsDetailsComponent implements OnInit {
  currentProgram = null;
  message ='';

  constructor(
    private upcommingProgramService:UpcommingProgramService,
    private route:ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.message='';
    this.getProgram(this.route.snapshot.paramMap.get('id'));
  }

  getProgram(id): void {
    this.upcommingProgramService.get(id)
    .subscribe(
      data=> {
        this.currentProgram=data;
        console.log(data);
      },
      error=>{
        console.log(error);
      }
    );
  }

  updatePublished(status): void{
    const data={
      programName: this.currentProgram.programName,
      programDescription: this.currentProgram.programDescription,
      programType: this.currentProgram.programType,
      programStartdate:this.currentProgram.programStartdate,
      programPrice:this.currentProgram.programPrice,
      published:status
    };

    this.upcommingProgramService.update(this.currentProgram.id,data)
    .subscribe(
      response =>{
        this.currentProgram.published = status;
        console.log(response);
      },
      error=>{
        console.log(error);
      }
    );
  }

  updateProgram():void{
    this.upcommingProgramService.update(this.currentProgram.id, this.currentProgram)
    .subscribe(
      response=>{
        console.log(response);
        this.message='The tutorial was updated successfully!';
      },
      error=>{
        console.log(error);
      }
    );
  }

  deleteProgram(): void{
    this.upcommingProgramService.delete(this.currentProgram.id)
    .subscribe(
      response=>{
        console.log(response);
        this.router.navigate(['/upcommingPrograms']);
      },
      error=>{
        console.log(error);
      }
    );
  }

}
