import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHomeProgramComponent } from './admin-home-program.component';

describe('AdminHomeProgramComponent', () => {
  let component: AdminHomeProgramComponent;
  let fixture: ComponentFixture<AdminHomeProgramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminHomeProgramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHomeProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
