import { Component, Input, OnInit } from '@angular/core';
import { FaqService } from 'src/app/services/faq.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-faq-add-update',
  templateUrl: './faq-add-update.component.html',
  styleUrls: ['./faq-add-update.component.css']
})
export class FaqAddUpdateComponent implements OnInit {
  questions: any;
  currentQuestion= null;
  currentIndex = -1;
  programName ='';
  isShown: boolean = false ;
  isEdit: boolean = false ;
  question = {
    question:'',
    standardAnswer:'',
    difficultyLevel:'',
    moduleId:1
  };
  message ='';
  submitted=false;
  searchText='';
  programsCopy:any;
  filteredArray=[];
  newarray=[];
  arr:any;
  items:any;
  constructor(private faqService: FaqService,
    private route:ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.retrievePrograms();
    this.message='';
    //this.getProgram(this.route.snapshot.paramMap.get('id'));
  }

  retrievePrograms(): void{
    this.faqService.getQuestions()
      .subscribe(
        data => {
          this.questions = data;
          this.items = this.questions.forEach(element => {

            this.newarray.push(element.question);

          });
           // for(var i=0;i<data.length;i++){
            //  this.items=item.programName;
           // }



          console.log(data);

          console.log(this.newarray);


        },
        error => {
          console.log(error);
        }
      );
  }

  showDiv={
    show:false
  }

  refreshList(): void {
    this.retrievePrograms();
    this.currentQuestion = null;
    this.currentIndex = -1;
  }

  setActiveQuestion(question, index): void {
    this.currentQuestion = question;
    this.currentIndex = index;

  }

 /* removeAllPrograms(): void {
    this.faqService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retrievePrograms();
        },
        error => {
          console.log(error);
        });
  }*/

 /* searchTitle(): void {
    this.faqService.findByProgramName(this.programName)
      .subscribe(
        data => {
          this.programs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }*/



  toggleShow() {

    this.isShown = true;

    }

    toggleShowedit(){
      this.isEdit=true;
    }
    saveQuestion(): void {
      const data = {
        question: this.question.question,
        standardAnswer: this.question.standardAnswer,
        difficultyLevel: this.question.difficultyLevel,
        moduleId: this.question.moduleId
      };

      this.faqService.createQuestions(data)
        .subscribe(
          response => {
            console.log(response);
            this.submitted = true;
          },
          error => {
            console.log(error);
          });
          window.location.reload();
          window.location.reload();
    }

    newQuestion(): void {
      this.submitted = false;
      this.question = {
        question: '',
        standardAnswer: '',
        difficultyLevel:'',
        moduleId:1
      };
    }

  /*  getProgram(id): void {
      this.faqService.get(id)
      .subscribe(
        data=> {
          this.currentProgram=data;
          console.log(data);
        },
        error=>{
          console.log(error);
        }
      );
    }*/

   // updateProgram():void{
     // this.faqService.update(this.currentProgram.id, this.currentProgram)
     // .subscribe(
     //   response=>{
      //    console.log(response);
      //    this.message='The tutorial was updated successfully!';
      //  },
      //  error=>{
       //   console.log(error);
      //  }
     // );
    //  window.location.reload();
   // }

    updateQuestion():void{
          this.items = this.questions.find(ele => ele.id==this.currentQuestion.id);
         // console.log(this.items.id);
//programs nu try pannanum

                this.items.id=this.currentQuestion.id;
                this.items.question=this.currentQuestion.question;
                this.items.standardAnswer=this.currentQuestion.standardAnswer;
                this.items.difficultyLevel=this.currentQuestion.difficultyLevel;
                this.items.moduleId=this.currentQuestion.moduleId;
              console.log(this.questions.id);
                this.faqService.createQuestions(this.items)
        .subscribe(
          response => {
            response.id=this.currentQuestion.id;
            console.log(response);

          },
          error => {
            console.log(error);
          });
          window.location.reload();

              //console.log(element.id);
              //element.question=this.currentProgram.question;
              //element.standardAnswer=this.currentProgram.standardAnswer;
             // console.log(element.standardAnswer);
             // element.difficultyLevel=this.currentProgram.difficultyLevel;
             // element.moduleId=this.currentProgram.moduleId;

          console.log(this.questions.question);

    }

  /*  deleteProgram(): void{
      this.faqService.delete(this.currentProgram.id)
      .subscribe(
        response=>{
          console.log(response);

        },
        error=>{
          console.log(error);
        }
      );
     // window.location.reload();
    }*/




}
