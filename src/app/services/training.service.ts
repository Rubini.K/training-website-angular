import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:8080/api/training';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {

  constructor(private http: HttpClient) { }

  getTrainings(): Observable<any> {
    return this.http.get(`${baseUrl}/trainings`);
  }
  getModulesList(): Observable<any> {
    return this.http.get(`${baseUrl}/modules`);
  }
}
