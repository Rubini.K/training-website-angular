import { FaqAddUpdateComponent } from './faq/faq-add-update/faq-add-update.component';
import { AppComponent } from './app.component';
import { AdminHomeProgramComponent } from './admin/admin-home-program/admin-home-program.component';
import { HomeSlidesComponent } from './home-slides/home-slides.component';
import { DevelopmentComponent } from './course/development/development.component';
import { ProgramsDetailsComponent } from './admin/programs-details/programs-details.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramsListComponent } from './admin/programs-list/programs-list.component';
import { AddProgramComponent } from './admin/add-program/add-program.component';
import { FaqSearchComponent } from './faq/faq-search/faq-search.component';
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'upcommingPrograms', component: ProgramsListComponent },
  { path: 'upcommingPrograms/:id', component: ProgramsDetailsComponent },
  { path: 'add', component: AddProgramComponent },
  { path: 'admin/add', component: AddProgramComponent },
  { path: 'development', component:DevelopmentComponent },
  { path: 'faq', component:FaqSearchComponent },
  { path: 'admin', component:FaqAddUpdateComponent },
  { path: 'home', component:HomeSlidesComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
